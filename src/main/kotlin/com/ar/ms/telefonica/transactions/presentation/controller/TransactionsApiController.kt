package com.ar.ms.telefonica.transactions.presentation.controller

import com.ar.ms.telefonica.transactions.domian.GetAllTransactions
import com.ar.ms.telefonica.transactions.domian.model.DomainResponse
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1")
class TransactionsApiController(val transacctions: GetAllTransactions) {

    @GetMapping("/transactions/all")
    fun transactionsAll(): DomainResponse{
        return transacctions.execute()
    }
}