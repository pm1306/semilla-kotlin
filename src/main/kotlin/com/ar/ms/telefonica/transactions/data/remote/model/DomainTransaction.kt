package com.ar.ms.telefonica.transactions.data.remote.model

data class DomainTransaction(
    val id: Int,
    val siteTransactionId:String,
    val payment_method_id: Int,
    val cardBrand: String,
    val amount: Int,
    val currency: String,
    val status: String,
    val statusDetails: String,
    val date: String,
    val customer: String,
    val bin: String?,
    val installments: Int?,
    val firstInstallmentExpirationDate: String?,
    val paymentType:String,
    val subPayments: List<String>?,
    val siteId:String,
    val fraudDetection:String,
    val aggregateData:String?,
    val establishmentName: String,
    val spv: String?,
    val confirmed:String,
    val pan:String?,
    val customerToken: String,
    val cardData:String,
    val emvIssuerData:String,
    val token:String,
    val operator: String
)  {
}