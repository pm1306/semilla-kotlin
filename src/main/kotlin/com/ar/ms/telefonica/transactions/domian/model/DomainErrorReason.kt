package com.ar.ms.telefonica.transactions.domian.model

data class DomainErrorReason(
    val id: Int,
    val description:String,
    val additionalDescription: String)  {
}