package com.ar.ms.telefonica.transactions.data

import com.ar.ms.telefonica.transactions.data.remote.model.ResponseRemote
import com.ar.ms.telefonica.transactions.data.source.ITransactionsService
import com.ar.ms.telefonica.transactions.domian.repository.ITransactions
import mu.KotlinLogging
import org.springframework.stereotype.Component

@Component
class Transactions(private val transactionsService: ITransactionsService):ITransactions {
    private val log = KotlinLogging.logger{}
    override fun findAllTransactions(): ResponseRemote {
        log.info { "Call llamando al service remote" }
        log.info { "Call llamando al componente Transactions" }
        return transactionsService.getTransactionAll()
    }
}