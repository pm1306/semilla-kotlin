package com.ar.ms.telefonica.transactions.domian.model

data class DomainError(val type: String, val reason: DomainErrorReason) {
}