package com.ar.ms.telefonica.transactions.domian.model

data class DomainStatusDetails(
    val ticket: String,
    val cardAuthorizationCode: String,
    val addressValidationCode:String,
    val error: DomainError) {
}