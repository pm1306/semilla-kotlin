package com.ar.ms.telefonica.transactions

import mu.KotlinLogging
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application
private val log = KotlinLogging.logger{}
fun main(args: Array<String>) {
	log.info { "Hello, Que hay de nuevo viejo.!!" }
	runApplication<Application>(*args)
}
