package com.ar.ms.telefonica.transactions.data.remote

import com.ar.ms.telefonica.transactions.data.remote.model.DomainTransaction
import com.ar.ms.telefonica.transactions.data.remote.model.ResponseRemote
import com.ar.ms.telefonica.transactions.data.source.ITransactionsService
import mu.KotlinLogging
import org.springframework.stereotype.Service


@Service
class TransactionsServiceImpl:ITransactionsService {

    private val log  = KotlinLogging.logger{}

    override fun getTransactionAll(): ResponseRemote {
        log.info { "Service Call Remote" }
        return ResponseRemote(listOf(
            DomainTransaction(  12215342, "0000018925",31,"Visa Débito",
                123400,"ars","approved",        "status_details",
                "2021-12-20T09:21Z", "Pedro Perez","451772", 1,
                null,"single", null,"99999966",
                "Sin Fraude", null, "establishment_name","confirmed",
                "3e4807024b60d60ab1154451c3bd0d1c75","customer_token","/tokens/12215342",
                "emv_issuer_data","26c14abc-93c4-4dfe-951c-cab860f80efc", "Pablo Perez",
                "Pablo Perez")
        ))
    }
}