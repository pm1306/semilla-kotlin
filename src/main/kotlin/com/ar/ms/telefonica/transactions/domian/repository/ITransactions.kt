package com.ar.ms.telefonica.transactions.domian.repository

import com.ar.ms.telefonica.transactions.data.remote.model.ResponseRemote

interface ITransactions {
    fun findAllTransactions(): ResponseRemote
}