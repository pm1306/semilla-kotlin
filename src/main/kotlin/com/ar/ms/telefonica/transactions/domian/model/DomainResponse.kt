package com.ar.ms.telefonica.transactions.domian.model

data class DomainResponse(val limit: Int?, val offset:Int?, val results: List<DomainTransaction>?) {
}