package com.ar.ms.telefonica.transactions.data.source

import com.ar.ms.telefonica.transactions.data.remote.model.ResponseRemote

interface ITransactionsService {

    fun getTransactionAll(): ResponseRemote
}