package com.ar.ms.telefonica.transactions.domian

import com.ar.ms.telefonica.transactions.domian.model.DomainResponse
import com.ar.ms.telefonica.transactions.domian.model.DomainTransaction
import com.ar.ms.telefonica.transactions.domian.repository.ITransactions
import mu.KotlinLogging
import org.springframework.stereotype.Service


@Service
class GetAllTransactions(private val transactionsData: ITransactions) {

    private val log = KotlinLogging.logger{}
    fun execute(): DomainResponse{
        log.info { "CALL al componente y servicio de Data" }
        //log.info { "RESPONSE : " + transactionsData.findAllTransactions()}
        log.info { "Ingresando al llamado de la capa de Dominio" }

        var reponseDomain = DomainTransaction("12345", "site-14",
        5, "VISA", 123.21,
            "ARG", "Aprobado", null,
            "2021-12-20T09:25Z",
        null, "bin", 2, "23", "payment", null, "siteId", "false", null, "argentina", null,
        "confirmado", "pan", "tokencliente", "cardData", null, "token",
            null)
        return DomainResponse(20, 5, listOf(reponseDomain))
        /*
        transactionsData.findAllTransactions().body?.forEach {
            log.info { "Ingresando al llamado de la capa de Dominio" }
            var reponseDomain : DomainTransaction(
                it.id,
                it.siteTransactionId,
                it.payment_method_id,
                it.cardBrand,
                it.amount,
                it.currency,
                it.bin,
                it.installments,
                it.firstInstallmentExpirationDate,
                it.paymentType,
                it.subPayments,
                it.siteId,
                it.fraudDetection,
                it.aggregateData,
                it.establishmentName,
                it.spv,
                it.confirmed,
                it.pan,
                it.customerToken,
                it.cardData,
                it.emvIssuerData,
                it.token,
                it.operator)
        }  */
/*
       transactionsData.findAllTransactions().body?.map {
            log.info { "Ingresando al llamado de la capa de Dominio" + it.aggregateData}
            reponseDomain(id = it.aggregateData)
        }*/
    }
}