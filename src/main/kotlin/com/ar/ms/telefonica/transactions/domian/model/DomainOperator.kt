package com.ar.ms.telefonica.transactions.domian.model

data class DomainOperator(val operatorName: String, val operatorId:String) {
}